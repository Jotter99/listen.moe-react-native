import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions,
} from 'react-native';

import Radio from './app/components/Radio'

const deviceHeight = Dimensions.get("window").height;

export default class listenmoe extends Component {
  render() {
    return (
      <View style={{flex:1, maxHeight: deviceHeight}}>
        <Radio/>
      </View>
    );
  }
}
AppRegistry.registerComponent('listenmoe', () => listenmoe);
