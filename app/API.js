/**
* Handles all API requests
* Also handles processing of data received from API e.g. creating list with search data, getting/saving user token
*/

import {AsyncStorage} from "react-native"

class API {
  /**
  * Retrieves token from storage
  * Checks if token has expired (valid if less than 28 days old)
  * @return {string|null} Authentication token || null undefined token
  */
  async getToken() {
    let token = AsyncStorage.getItem("authToken");
    let time = await AsyncStorage.getItem("lastAuth");
    time = JSON.parse(time);
    if(time == undefined || token == undefined) return null;
    let currentTime = Date.now();
    if(currentTime - time > 2419200000) return null;
    return token;
  }
  /**
  * Authenticates the User
  * @param {string} username
  * @param {string} password
  * @return {string} auth token
  */
  async login(username, password) {
    try {
      let body = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);
      let response = await fetch("https://listen.moe/api/authenticate", {
        method: "post",
        headers: {
          "Accept": "*/*",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: body
      });
      let responseJSON = await response.json();
      if(responseJSON.success == true) {
        this.saveToken(responseJSON.token);
      }
      return responseJSON;
    } catch(error) {
      console.log(error)
      return {success: false, message: "An error occurred performing this Action"};
    }
  }
  /**
  * Saves the token to storage
  * @param {string} token authentication token
  */
  async saveToken(token) {
    try {
      AsyncStorage.setItem("authToken", token);
      AsyncStorage.setItem("lastAuth", JSON.stringify(Date.now()));
    } catch(error) {
      console.log(error);
    }
  }
  /**
  * Search request
  * @param {string} query Search query
  * @param {boolean} favoritesOnly return favorites only - requests filtering
  * @return {Object} See createList(json) for the object format
  */
  async search(query, favoritesOnly) {
    let token = await this.getToken();
    if(token == undefined) {
      return null;
    }
    try {
      let body = "query=" + encodeURIComponent(query)
      let response = await fetch("https://listen.moe/api/songs/search",{
      method: "post",
      headers: {
        "authorization": token,
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: body
    });
    let responseJSON = await response.json();
    return this.createList(responseJSON,false, (favoritesOnly === true));
    } catch (error) {
      console.log("Search: " + error)
      return null;
    }
  }
  /**
  * Creates JSON to be displayed in a listView
  * NOTE There is probably a better way to parse this data, but ¯\_(ツ)_/¯ this does what it needs to reasonably quickly
  * @param {string} json json received from search requests or user favorites request
  * @param {boolean} override set all songs to favorite status true. Used for user favorites list
  * @param {boolean} favoritesOnly returns favorites only - for filtering
  * @return {Object} formatted { data:{"Song 1", "Song 2", "..."}, enabled:{true,false,...}, favorite:{true,false,...}, id:{123,321,...}, requests:3}
  */
  createList(json, override, favoritesOnly) {
    let songsList = {data: [], enabled: [], favorite: [], id: []};
    for(var i = 0; i < json.songs.length; i++) {
      // Whether song is a favorite //
      if((json.songs[i].favorite != undefined && json.songs[i].favorite == 1) || override)
        songsList.favorite[i] = true;
      else {
        if(favoritesOnly) continue;
        songsList.favorite[i] = false;
      }
      // Text to display //
      if(json.songs[i].anime != '' && json.songs[i].anime != undefined)
        songsList.data[i] = json.songs[i].artist + " - " + json.songs[i].title + " [ " + json.songs[i].anime + " ]";
      else
        songsList.data[i] = json.songs[i].artist + " - " + json.songs[i].title;
      // Whether song is enabled //
      if(json.songs[i].enabled != undefined && json.songs[i].enabled == false)
        songsList.enabled[i] = false;
      else
        songsList.enabled[i] = true;
      // Song id //
      songsList.id[i] = json.songs[i].id;
    }
    // Number of requests remaining
    songsList.requests = json.extra.requests;
    return songsList;
  }
  /**
  * Sends a Request request
  * @param {integer} songID song id
  * @return {Object|null} Object format - {success: boolean, message: string, id: integer}
  */
  async request(songID) {
    let token = await this.getToken();
    try {
      let body = "song=" + encodeURIComponent(songID)
      let response = await fetch("https://listen.moe/api/songs/request",{
      method: "post",
      headers: {
        "authorization": token,
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: body
    });
    let responseJSON = await response.json();
    // Return song id as well. Due to async nature the songsList may have changed while making request - Shouldn't happen but ¯\_(ツ)_/¯, can never be too safe
    responseJSON.id = songID;
    return responseJSON;
    } catch (error) {
      console.log(error)
      return null;
    }
  }
  /**
  * Sends a Favorite request
  * @param {integer} id song id
  * @return {object|null} Object format - {success: boolean, message: string, id: integer}
  */
  async favorite(songID) {
    let token = await this.getToken();
    try {
      let body = "song=" + encodeURIComponent(songID)
      let response = await fetch("https://listen.moe/api/songs/favorite",{
      method: "post",
      headers: {
        "authorization": token,
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: body
    });
    let responseJSON = await response.json();
    // Return song id as well. Due to async nature the songsList/song may have changed while making request - Shouldn't happen but ¯\_(ツ)_/¯, can never be too safe
    responseJSON.id = songID;
    return responseJSON;
    } catch (error) {
      console.log(error)
      return null;
    }
  }
  /**
  * Get list of user favorites
  * @return {Object} formatted { data:{"Song 1", "Song 2", "..."}, enabled:{true,false,...}, favorite:{true,false,...}, id:{123,321,...}, requests:3}
  */
  async favorites() {
    let token = await this.getToken();
    try {
      let response = await fetch("https://listen.moe/api/user/favorites",{
      method: "get",
      headers: {
        "authorization": token,
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded"
      },
    });
    let responseJSON = await response.json();
    return this.createList(responseJSON, true);
    } catch (error) {
      console.log(error)
      return null;
    }
  }
}
module.exports = API;
