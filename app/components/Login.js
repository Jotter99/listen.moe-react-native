'use strict'
import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TextInput,
  Dimensions,
  Image,
  Platform,
  PixelRatio,
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome"

import API from "../API.js"
import styles from "./Styles.js"

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const api = new API()

/**
* Displays the Login dialog
*/
class Login extends Component {
  constructor(props){
    super(props);
    this.state = {username: '', password: ''};
  }
  render(){
    return(
      <View style={styles.loginDialog}>
        <View style={styles.titleParent}>
          <Text style={styles.title}>    Login</Text>
          <Icon name="times" color="#FFFFFF" style={styles.close} size={25} onPress={()=>{this.props.onCancel()}}/>
        </View>
        <TextInput placeholder="Username" onChangeText={(text)=>{this.setState({username: text})}} placeholderTextColor="#D3D3D3" autoCorrect={false} underlineColorAndroid="#EC1A55" selectionColor="#EC1A55" style={styles.textInput}/>
        <TextInput placeholder="Password" onChangeText={(text)=>{this.setState({password: text})}} placeholderTextColor="#D3D3D3" autoCorrect={false} underlineColorAndroid="#EC1A55" selectionColor="#EC1A55" style={styles.textInput} secureTextEntry={true}/>
        {(this.state.error == undefined)?null:<Text style={styles.loginError}>{this.state.error}</Text>}
        <View style={styles.loginButton}>
          <Text style={styles.loginButtonText} onPress={()=>{this.login()}} numberOfLines={1}>LOGIN</Text>
        </View>
        <Image style={{alignSelf: "center"}} source={require("../drawables/kanna.gif")}/>
      </View>
    )
  }
  /**
  * Authenticates user
  */
  async login() {
    if(this.state.username == '' || this.state.password == '') return;
    let responseJSON = await api.login(this.state.username, this.state.password);
    if(responseJSON.success) {
      console.log(responseJSON.token); //TODO: Remove - Testing Only
      this.props.onLogin(responseJSON.token);
    } else {
      this.setState({error: responseJSON.message});
    }
  }
}
module.exports = Login
