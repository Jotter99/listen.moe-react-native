'use strict'
import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TextInput,
  Dimensions,
  Platform,
  PixelRatio,
  ListView,
  TouchableOpacity,
  StatusBar,
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome"

import CustomStatusBar from "./StatusBar.js"
import API from "../API.js"
import styles from "./Styles.js"

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

const deviceWidth = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const api = new API()

var songsList = {};
/**
* Displays the Requests/Favorites screen
*/
class RequestsFavorites extends Component {
  constructor(props) {
    super(props);
    this.state = {listData: ds.cloneWithRows([])};
  }
  componentDidMount() {
    if(this.props.page == 2) {
      this.favorites();
    }
  }

  render() {
    return(
      <View style={styles.container}>
        <CustomStatusBar/>
        <View style={styles.requestsFavoritesContainer}>
        <View style={styles.titleParent}>
          <Text style={styles.title}>    {(this.props.page == 1)? "Requests": "Favorites"}</Text>
          <Icon name="times" color="#FFFFFF" style={styles.close} size={25} onPress={()=>{this.props.onCancel()}}/>
        </View>
        <TextInput placeholder="Search" style={styles.textInput} onChangeText={(text)=>{this.setState({query: text})}} onSubmitEditing={()=>{if(this.state.query != undefined){this.search(this.state.query)}}} returnKeyType={"search"} placeholderTextColor="#D3D3D3" autoCorrect={false} underlineColorAndroid="#EC1A55" selectionColor="#EC1A55" autoCapitalize={"none"}/>
        {this.showRemaining()}
        <ListView style={(Platform.OS == "android")?styles.listAndroid:styles.listIOS} dataSource={this.state.listData} renderRow={(rowData, sec, i)=> this.genRow(rowData, i)} enableEmptySections={true}/>
        {(this.state.selection == undefined)?null: <TouchableOpacity style={styles.dialogShadow} onPress={()=>{this.setState({selection: null})}} activeOpacity={1.0}/>}
        {this.actionDialog()}
        </View>
      </View>
    )
  }
  /**
  * Whether to show/What to show for - Requests remaining text
  * @return {Component|null} Requests Text
  */
  showRemaining() {
    if(this.state.requests != undefined){
      return (
        <View>
          <Text style={styles.requestsText}>
            <Text>Requests Remaining: </Text>
            <Text>{this.state.requests}</Text>
          </Text>
        </View>
      )
    } else {
      return null;
    }
  }
  /**
  * Get list of user favorites
  */
  async favorites() {
    songsList = await api.favorites();
    if(songsList != null) {
      this.setState({requests: songsList.requests, listData: ds.cloneWithRows(songsList.data)});
    }
  }
  /**
  * Make a search request
  * @param {string} query Search Query
  */
  async search(query) {
    if((query == "" || query == undefined) && this.props.page == 2) {
      this.favorites();
    } else if (query == undefined || query == "") {
      return;
    }
    songsList = await api.search(query,(this.props.page == 2));
    if(songsList != null) {
      this.setState({requests: songsList.requests,listData: ds.cloneWithRows(songsList.data)});
    } else {
      //TODO: Make error clear
    }
  }
  /**
  * This controls what is shown for each row of the listView and how it is styled
  * Song styles can be 1 of 4  - normal (Enabled & Not Favorite), fav (Enabled & Favorite), disabled (Disabled & Not Favorite), disabledFav (Disabled & Favorite)
  * @param {string} rowData Text to be displayed in the row
  * @param {integer} i index of the row
  * @return {null|Component} null| Row component
  */
  genRow(rowData, i) {
    if(this.props.page == 2 && !songsList.favorite[i]) return null;
    // Init all rows as normal
    let style = styles.normal;
    // Set default background color of view contained text
    let background = "#1D1F2B";
    // Slightly change background color of every second row because it looks cool
    if(i % 2 == 0) {
      background = "#1A1B26"
    }
    // disabled
    if(songsList.enabled[i] == 0 && songsList.favorite[i] == 0){
      style = styles.disabled;
    } // disabledFav - Requests screen only
    else if(songsList.enabled[i] == 0 && songsList.favorite[i] == 1 && this.props.page != 2) {
      style = styles.disabledFav;
      background = "#EC1A55";
    } // fav - Requests screen only
    else if(songsList.enabled[i] == 1 && songsList.favorite[i] == 1 && this.props.page != 2) {
      style = styles.fav;
      background = "#EC1A55";
    }
    return (
      <View style={{marginBottom: 1, minHeight: 50, justifyContent: "center", backgroundColor: background, borderRadius: 3}}>
        <Text style={style} onPress={()=>{this.setState({selection: i})}}>{rowData}</Text>
      </View>
    )
  }
  /**
  * Whether to show/What to show for - Request Dialog
  * P.S. https://facebook.github.io/react-native/docs/alert.html Let's pretend this wasn't a thing the whole time
  * @return {null|Component} null|Dialog Component
  */
  actionDialog() {
    let index = this.state.selection;
    if(index == null) {
      return null;
    } else {
      return(
        <View style={styles.requestsFavoritesDialog}>
          <Text style={styles.dialogQuestion}>What action would you like to take?</Text>
          <View style={styles.dialogBottom}>
            {(songsList.enabled[index]) ? <Text style={styles.dialogChoice} onPress={()=>{this.request(index)}}>Request</Text>: null}
            <Text style={styles.dialogChoice} onPress={()=>{this.favorite(index)}}>{(songsList.favorite[index]) ? "Unfavorite": "Favorite"}</Text>
            <Text style={styles.dialogChoice} onPress={()=>{this.setState({selection: null, error: null})}}>Cancel</Text>
          </View>
          {(this.state.error == undefined)?null:<Text style={styles.requestsFavoritesError}>{this.state.error}</Text>}
        </View>
      )
    }
  }
  /**
  * Sends a Request request
  * @param {integer} index index of song in listview, corresponds to index of song id in id array
  */
  async request(index) {
    let result = await api.request(songsList.id[index]);
    if(result == null) { // Check if http request errored
      this.setState({error: "There was an error performing this action"});
    } else {
      if(result.success) { // If song successfully requested
        if(songsList.id[index] == result.id) { // Check that the current displaying list matches the list the request was made from
          songsList.enabled[index] = false;
          // Re-Render listView, decrement number of requests remaining, hide dialog
          this.setState({listData: ds.cloneWithRows(songsList.data),requests: this.state.requests -= 1, selection: null, error: null});
        }
      } else {
        this.setState({error: "Error: " + result.message});
      }
    }
  }
  /**
  * Sends a favorite request
  * @param {integer} index index of song in listview, corresponds to index of song id in id array
  */
  async favorite(index) {
    let result = await api.favorite(songsList.id[index]);
    if(result == null) { // Check if http request errored
      this.setState({error: "There was an error performing this action"});
    } else {
      if(result.success) { // If favorite status was changed correctly
        if(songsList.id[index] == result.id) { // Check that the current displaying list matches the list the request was made from
          if(this.props.page == 2 && !result.favorite) { // If unfavoriting from favorites list remove song from list, else toggle status
            songsList.data.splice(index,1);
            songsList.enabled.splice(index,1);
            songsList.id.splice(index, 1);
          } else {
            songsList.favorite[index] = result.favorite;
          }
          // Re-Render listView, hide dialog
          this.setState({listData: ds.cloneWithRows(songsList.data), selection: null, error: null});
        }
      } else {
        this.setState({error: "Error: " + result.message});
      }
    }
  }
}
module.exports = RequestsFavorites
