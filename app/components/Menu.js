"use strict"

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  TouchableHighlight,
  TextInput,
  ListView,
  Button,
} from "react-native";

import CustomStatusBar from "./StatusBar"

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
var songsList = {};

class Menu extends Component {
  //tab: React.PropTypes.string.isRequired,
  static propTypes = {

  }
  constructor(props) {
    super(props);
    this.state={tab:"request", authToken:"" , username: "", password: "",searchData:ds.cloneWithRows([]), favoritesData: ds.cloneWithRows([])}
  }
  componentDidMount() {
    this.getToken();
  }
  render() {
    return(
      <View style={styles.container}>
        <CustomStatusBar/>
        <View style={styles.navBar}>
          <TouchableHighlight underlayColor="#EC1A552F" style={this.navStyleView("request")} onPress={()=>{if(this.state.tab != "request"){this.setState({tab:"request", searchData:ds.cloneWithRows([])}); songsList = {};}}}>
            <Text style={this.navStyleText("request")}>REQUESTS</Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="#EC1A552F" style={this.navStyleView("favorites")} onPress={()=>{if(this.state.tab != "favorites"){this.setState({tab:"favorites", favoritesData:ds.cloneWithRows([])});songsList = {};}}}>
            <Text style={this.navStyleText("favorites")}>FAVORITES</Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="#EC1A552F" style={this.navStyleView("login")} onPress={()=>{if(this.state.tab != "login"){this.setState({tab:"login"})}}}>
            <Text style={this.navStyleText("login")}>LOGIN</Text>
            </TouchableHighlight>
        </View>
    {this.requestTab()}
    {this.favoritesTab()}
    {this.loginTab()}
    </View>
    );
  }
  /**
  * Styling for tabs
  * @param {tab} str Tab identifier to check if selected
  * @return Stlying for tab
  */
  navStyleView(tab) {
    if(this.state.tab === tab)
    return styles.navViewSelected;
    else
      return styles.navView;
  }
  /**
  * Styling for tab text
  * @param {tab} str Tab identifier to check if selected
  * @return Styling for tab text
  */
  navStyleText(tab) {
    if(this.state.tab === tab)
    return styles.navTextSelected;
    else
      return styles.navText;
  }
  /**
  * Whether to show/what to show for - request tab
  * @return react-component
  */
  requestTab() {
    if(this.state.tab != "request") {
      return null;
    }
    else if(this.state.authToken === '') {
      return(
      <Text style={styles.loginWarn}>YOU MUST BE LOGGED IN TO ACCESS THIS FEATURE</Text>
      )
    } else {
      return(
      <View style={styles.requestTab}>
        <TextInput placeholder="Search" onSubmitEditing={()=>{if(this.state.query != undefined){this.search(this.state.query)}}} onChangeText={(text)=>{this.setState({query: text})}} returnKeyType={"search"} placeholderTextColor="#D3D3D3" autoCorrect={false} underlineColorAndroid="#EC1A55" selectionColor="#EC1A55" style={styles.textInput}/>
        <ListView style={styles.listViews} dataSource={this.state.searchData} renderRow={(rowData, sec, i)=> this.genRow(rowData, sec, i)} enableEmptySections={true}/>
      </View>
      )
    }
  }
  /**
  * Whether to show/what to show for - favorites tab
  * @return react-component
  */
  favoritesTab() {
    if(this.state.tab != "favorites"){
      return null;
    }
    if(this.state.authToken === '') {
      return(
      <Text style={styles.loginWarn}>YOU MUST BE LOGGED IN TO ACCESS THIS FEATURE</Text>
      )
    } else {
      this.favorites();
      return(
        <View style={styles.favoritesTab}>
        <ListView style={styles.listViews} dataSource={this.state.favoritesData} renderRow={(rowData, sec, i)=> this.genRow(rowData, sec, i)} enableEmptySections={true}/>
      </View>
      )
    }
  }
  /**
  * Whether to show/what to show for - login tab
  * @return react-component
  */
  loginTab() {
    if(this.state.tab != "login") {
      return null;
    }
    if(this.state.authToken == '') {
      return(
      <View style={styles.loginTab}>
        <TextInput placeholder="Username" onChangeText={(text)=>{this.setState({username: text})}} placeholderTextColor="#D3D3D3"autoCorrect={false} underlineColorAndroid="#EC1A55" selectionColor="#EC1A55" style={styles.textInput}/>
        <TextInput placeholder="Password" onChangeText={(text)=>{this.setState({password: text})}} placeholderTextColor="#D3D3D3"autoCorrect={false} underlineColorAndroid="#EC1A55" selectionColor="#EC1A55" style={styles.textInput} secureTextEntry={true}/>
        <Text style={styles.loginButton} onPress={()=>{this.login()}}>LOGIN</Text>
      </View>
      )
    } else {
      return (
      <Text style={styles.loginButton} onPress={()=>{this.logout()}}>LOGOUT</Text>
      )
    }
  }
  /**
  * Retrieve token from storage
  */
  async getToken() {
    try {
      let token = await AsyncStorage.getItem("authToken");
      if(token != undefined) {
        this.setState({authToken: token})
      }
    } catch(error) {}
  }
  /**
  * Make search request
  * @param {query} str Search Query
  */
  async search(query) {
    if(this.state.authToken == undefined) return;
    try {
      let body = "query=" + encodeURIComponent(query)
      let response = await fetch("https://listen.moe/api/songs/search",{
      method: "post",
      headers: {
        "authorization": this.state.authToken,
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: body
    });
    let responseJSON = await response.json();
    this.createList(responseJSON, "search");
  } catch (error) {console.log(error)}
  }
  /**
  * Retrieve favorites list
  */
  async favorites() {
    if(this.state.authToken == undefined) return;
    try {
      let response = await fetch("https://listen.moe/api/user/favorites",{
        method: "get",
        headers: {
          "authorization": this.state.authToken,
          "Accept": "*/*",
          "Content-Type": "application/x-www-form-urlencoded"
        }
      });
      let responseJSON = await response.json();
      this.createList(responseJSON, "favorites");
    } catch(error){console.log(error)}
  }
  /**
  * Creates JSON object containing arrays of song info
  * @param {json} str json song data to be processed
  * @param {tab} str which tab is the data for
  */
  createList(json, tab) {
    songsList = {row: [], enabled: [], favorite: [], id: []};
    for(var i = 0; i < json.songs.length; i++) {
      // Text to display //
      if(json.songs[i].anime != '' && json.songs[i].anime != undefined)
        songsList.row[i] = json.songs[i].artist + " - " + json.songs[i].title + " [ " + json.songs[i].anime + " ]";
      else
        songsList.row[i] = json.songs[i].artist + " - " + json.songs[i].title;
      // Whether song is enabled //
      if(json.songs[i].enabled != undefined && json.songs[i].enabled == false)
        songsList.enabled[i] = 0;
      else
        songsList.enabled[i] = 1;
        // Whether song is a favorite //
      if(json.songs[i].favorite != undefined && json.songs[i].favorite == 1)
        songsList.favorite[i] = 1;
      else
        songsList.favorite[i] = 0;
      // Song id //
      songsList.id[i] = json.songs[i].id;
    }
    if(tab == "search")
      this.setState({searchData: ds.cloneWithRows(songsList.row)})
    else if(tab == "favorites")
      this.setState({favoritesData: ds.cloneWithRows(songsList.row)})
  }
  /**
  * ListView row creation
  * @param {rowData} str Text displayed by the row
  * @param {sec} int section id
  * @param {i} int row id
  * @return ListView row react-component
  */
  genRow(rowData, sec, i) {
    let style = styles.normal;
    let background = "#EC1A5500";
    if(songsList.enabled[i] == 0 && songsList.favorite[i] == 0)
    style = styles.disabled;
    else if(songsList.enabled[i] == 0 && songsList.favorite[i] == 1) {
      style = styles.disabledFav;
      background = "#EC1A55";
    }
    else if(songsList.enabled[i] == 1 && songsList.favorite[i] == 1) {
      style = styles.fav;
      background = "#EC1A55";
    }
    return (
      <View style={{borderBottomWidth: 1, borderBottomColor: "#1a1b26", marginBottom: 5, minHeight: 50, justifyContent: "center", backgroundColor: background}}>
        <Text style={style} onPress={()=>{console.log(i)}} >{rowData}</Text>
      </View>
    )
  }
  /**
  * Fetches authentication token
  */
  async login() {
    if(this.state.username == '' || this.state.password == '') return;
    try {
      let body = "username=" + encodeURIComponent(this.state.username) + "&password=" + encodeURIComponent(this.state.password);
      let response = await fetch("https://listen.moe/api/authenticate", {
        method: "post",
        headers: {
          "Accept": "*/*",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: body
      });
      let responseJSON = await response.json();
      if(responseJSON.success == true) {
        this.setState({authToken: responseJSON.token});
        this.saveToken(responseJSON.token);
      } else {console.log(responseJSON);}
    } catch(error) {console.log(error)}
  }
  /**
  * Sets all authentication data to null
  */
  async logout() {
    try {
      this.setState({authToken: ''});
      AsyncStorage.setItem("authToken", '')
      AsyncStorage.setItem("lastAuth", 0)
    } catch(error) {}
  }
  /**
  * Saves token to storage
  * @param {token} str User token
  */
  async saveToken(token) {
    try {
      AsyncStorage.setItem("authToken", token);
      AsyncStorage.setItem("lastAuth", JSON.stringify(Date.now()));
    } catch(error) {}
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#1D1F2B"
  },
  navBar: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  navViewSelected: {
    height: 50,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#EC1A55",
  },
  navView: {
    height: 50,
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  navTextSelected: {
    fontSize: 15,
    color: "#EC1A55",
    fontFamily: "OpenSans",
  },
  navText: {
    fontSize: 15,
    color: "#D3D3D3",
    fontFamily: "OpenSans",
  },
  textInput: {
    color: "#FFFFFF",
    fontSize: 18,
  },
  listViews: {
    maxHeight: deviceHeight - 150,
  },
  loginWarn: {
    margin: 20,
    color: "#FFFFFF",
    fontSize: 20,
    alignSelf: "center",
    textAlign: "center",
  },
  requestTab: {
    marginTop: 10,
    marginHorizontal: 20,
  },
  normal: {
    color:"#FFFFFF",
    fontSize: 18,
    fontFamily:"OpenSans",
  },
  fav: {
    color:"#FFFFFF",
    fontSize: 18,
    fontFamily:"OpenSans",
  },
  disabled: {
    color:"#A9A9A9",
    fontSize: 18,
    fontFamily:"OpenSans",
  },
  disabledFav: {
    color:"#A9A9A9",
    fontSize: 18,
    fontFamily:"OpenSans",
  },
  favoritesTab: {
    marginTop: 10,
    marginHorizontal: 20,
  },
  loginTab: {
    marginTop: 10,
    marginHorizontal: 20,
  },
  loginButton: {
    color: "#FFFFFF",
    paddingVertical: 5,
    textAlign: "center",
    justifyContent: "center",
    marginHorizontal: 75,
    marginTop: 5,
    fontFamily: "OpenSans",
    fontSize: 20,
    backgroundColor: "#EC1A55"
  },
})
module.exports = Menu
