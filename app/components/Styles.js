"use strict"
import {
  StyleSheet,
  Platform,
  PixelRatio,
  StatusBar,
  Dimensions,
} from "react-native"

const deviceWidth = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

/**
* Adjusts elements based on platform. Works pretty well
* @return {integer} rounded pixel size
*/
function scale(size) {
  if(Platform.OS == 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(size));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size)) + 3;
  }
}

const styles = StyleSheet.create({
  radioContainer: {
    flex: 1,
    backgroundColor: "transparent", // Transparency for IOS
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover", // "stretch" will squeeze entire width to fit screen
    maxWidth: deviceWidth,
    alignItems: 'center',
  },
  radio: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    marginHorizontal: 20,
  },
  logo: {
    maxWidth: deviceWidth - 40,
    resizeMode: "contain",
    paddingTop: height / 3,
  },
  playingContainer: {
    alignItems: "center",
    backgroundColor:'pink',
  },
  playing: {
    color: "#FFFFFF",
    textAlign: "center",
    fontSize: scale(20),
    fontFamily: "OpenSans",
  },
  radioBottom: {
    justifyContent: "flex-end",
    minWidth: deviceWidth - 40,
    alignItems: "center",
  },
  requestedContainer: {

  },
  requestedText: {
    color: "#FFFFFF",
    fontSize: scale(13),
    fontFamily: "OpenSans",
  },
  requestedUrl: {
    color: "#EC1A55",
    textDecorationLine: "underline",
    fontFamily: "OpenSans",
    fontSize: scale(13),
  },
  volumeContainer: {
    flexDirection: "row",
    marginHorizontal: 20,
  },
  volumeDown: {
    alignSelf: "flex-start",
  },
  slider: {
    alignSelf: "center",
    flex: 1,
  },
  volumeUp: {
    alignSelf: "flex-end",
  },
  controls: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  playPause: {
    marginRight: 10,
  },
  favorite: {
    marginLeft: 10,
  },
  bottomText: {
    minWidth: deviceWidth,
    flexDirection: 'row',
    marginBottom: 20,
  },
  poweredContainer: {
    textAlign: 'left',
    marginLeft: 20,
    flex:1,
  },
  poweredText: {
    color: "#FFFFFF",
    fontFamily: "OpenSans",
    fontSize: scale(10),
  },
  poweredUrl: {
    color: "#EC1A55",
    textDecorationLine: "underline",
    fontFamily: "OpenSans",
    fontSize: scale(10),
  },
  listenersContainer: {
    flex:1,
    marginRight: 20,
    textAlign: "right",
  },
  listenersText: {
    color: "#FFFFFF",
    fontFamily: "OpenSans",
    fontSize: scale(10),
  },
  dialogShadow: {
    position: "absolute",
    minWidth: deviceWidth,
    alignSelf: "flex-start",
    minHeight: height,
    backgroundColor: "#000000AA",
  },
  loginDialog: {
    top: (Platform.OS == "ios") ? 20: 40,
    minWidth: deviceWidth - 60,
    paddingVertical: 10,
    alignSelf: "center",
    margin: 10,
    position: "absolute",
    backgroundColor: "#1D1F2B",
    borderWidth: 4,
    borderRadius: 10,
    borderColor: '#EC1A55',
  },
  titleParent: {
    margin: 5,
    flexDirection: "row",
    alignItems: 'flex-start',
  },
  title: {
    color: "#FFFFFF",
    flex:1,
    fontSize: scale(20),
    fontFamily: "OpenSans",
    textAlign: "center",
  },
  close: {

  },
  textInput: {
    color: "#FFFFFF",
    borderBottomWidth: (Platform.OS == 'ios') ? 1 : 0,
    borderBottomColor: "#EC1A55",
    fontSize: scale(16),
    fontFamily: 'OpenSans',
    margin: 5,
    marginBottom: (Platform.OS == "ios")?0:5,
    height: scale(48),
  },
  loginError: {
    textAlign: "center",
    color: "#FFFFFF",
    fontSize: scale(13),
  },
  loginButton: {
    marginHorizontal: 40,
    backgroundColor: "#EC1A55",
    borderWidth : 1,
    borderColor: "#EC1A55",
    borderRadius: 10,
    paddingVertical: 5,
    paddingHorizontal: 60,
    justifyContent: "center",
    margin: 5,

  },
  loginButtonText: {
    color: "#FFFFFF",
    fontFamily: "OpenSans",
    fontSize: scale(18),
    textAlign: "center",
  },
  container: {
    flex:1,
    backgroundColor: "#1D1F2B",
    minWidth: deviceWidth,
    minHeight:  (Platform.OS == "android") ? height - StatusBar.currentHeight : height,
    maxHeight:  (Platform.OS == "android") ? height - StatusBar.currentHeight : height,
    position: "absolute",
  },
  requestsFavoritesContainer: {
    paddingHorizontal: 10,
    flex:1,
  },
  listAndroid: {
    height: height,
  },
  listIOS: {
    flex:1,
  },
  requestsText: {
    color:"#EC1A55",
    backgroundColor: "#1A1B26",
    fontFamily:"OpenSans",
    fontSize: scale(15),
    textAlign: "center",
    marginBottom: 5,
  },
  normal: {
    color:"#FFFFFF",
    fontSize: scale(16),
    fontFamily:"OpenSans",
  },
  fav: {
    color:"#FFFFFF",
    fontSize: scale(16),
    fontFamily:"OpenSans",
  },
  disabled: {
    color:"#A9A9A9",
    fontSize: scale(16),
    fontFamily:"OpenSans",
  },
  disabledFav: {
    color:"#A9A9A9",
    fontSize: scale(16),
    fontFamily:"OpenSans",
  },
  requestsFavoritesDialog: {
    position: "absolute",
    minWidth: deviceWidth - 60,
    alignSelf: "center",
    backgroundColor: "#FFFFFF",
    paddingVertical: 10,
    marginTop: height / 3,
    borderRadius: 10,
  },
  dialogQuestion: {
    fontFamily: "OpenSans",
    fontSize: scale(15),
    textAlign: "center",
    paddingTop: 10,
    marginBottom: 20,
    color: "#000000",
  },
  dialogBottom: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  dialogChoice: {
    fontFamily: "OpenSans",
    fontSize: scale(15),
    color: "#EC1A55",
  },
  requestsFavoritesError: {
    textAlign: "center",
    marginTop: 5,
    paddingBottom: 5,
  }
})
module.exports = styles
