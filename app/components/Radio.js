"use strict"

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Slider,
  Linking,
  AsyncStorage,
  Platform,
  NativeAppEventEmitter,
  PixelRatio,
  TouchableOpacity,
  StatusBar,
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome"

import API from "../API.js"
import styles from "./Styles.js"

import CustomStatusBar from "./StatusBar"
import NativeInterface from "../Native.js"
import Login from "./Login.js"
import RequestsFavorites from "./RequestsFavorites.js"

const deviceWidth = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const api = new API()
var nativeListener;

/**
* NOTE
* These were originally temporary but seem to work well
* Tested on one IOS device and one Android device
* (More testing required across range of devices)
* Leaving them here for now until a better scaling method is found - if necessary
*/
const scaling = {
  "volumeDown": deviceWidth / 7,
  "volumeUp": deviceWidth / 7.5,
  "playPause": deviceWidth / 9,
  "favorite": deviceWidth / 8,
}
/**
* Crossplatform UI & associated Code
*/
class Radio extends Component {
  constructor() {
    super();
    this.state = {status: false, playingText: "Connecting...",volume: 0.5, socket: {listeners: 0}, login: 0, requestsFavorites: 1}; // status indicates playing status.
  }
  componentWillMount() {
    console.log("Mounting...")
    if(nativeListener == undefined) {
      console.log("Creating Listener..")
      nativeListener = NativeAppEventEmitter.addListener("native", (event:Event)=>{this.nativeEmitters(event)});
    }
   }
  componentDidMount() {
    console.log("Mounted")
    this.getVolume();
    StatusBar.setBackgroundColor("#EC1A55");
    NativeInterface.startConnection() // TODO: IOS WebSocket
  }
  componentWillUnmount() {
    console.log("Unmounting...")
    nativeListener.remove();
  }
  /* NOTE
  * Volume slider minimumTrackTintColor and maximumTrackTintColor seem to swap on android and ios
  * Temp fix using platform check but should be noted in event of future fix.
  */
  render() {
    return (
      <View style={styles.radioContainer}>
        <CustomStatusBar/>
        <Image style={styles.backgroundImage} source={require("../drawables/images/background.jpg")}>
          <Image style={styles.logo} source={require("../drawables/images/logo.png")}/>
          <View style={styles.radio}>
            <View sytle={styles.playingContainer}>
              <Text style={styles.playing}>{this.state.playingText}</Text>
              <Text style={styles.playing}>{this.state.socket.artist_name}</Text>
              <Text style={styles.playing}>{this.state.socket.song_name}</Text>
              {this.showAnime()}
            </View>
          </View>
          <View style={styles.radioBottom}>
            {this.showRequested()}
            <View style={styles.volumeContainer}>
              <Icon style={styles.volumeDown} name="volume-down" color="#FFFFFF" size={scaling.volumeDown}/>
              <Slider style={styles.slider} value={this.state.volume} onSlidingComplete={(value) => {this.saveVolume(value)}} onValueChange={(value) => {NativeInterface.updateVolume(value)}} thumbTintColor={"#EC1A55"}
                maximumTrackTintColor={(Platform.OS == "ios")? "#F75E7E": "#EC1A55"} minimumTrackTintColor={(Platform.OS == "ios") ? "#EC1A55": "#F75E7E"}/>
              <Icon style={styles.volumeUp} name="volume-up" color="#FFFFFF" size={scaling.volumeUp}/>
            </View>
            <View style={styles.controls}>
              <View style={styles.playPause}>
                <Icon.Button name={(this.state.status == 0) ? "play" : "pause";} onPress={() => {NativeInterface.playRadio(!this.state.status)}} color="#FFFFFF" size={scaling.playPause} backgroundColor="#FFFFFF00"/>
              </View>
              <View style={styles.favorite}>
                <Icon.Button name={(this.state.socket.extended == undefined || this.state.socket.extended.favorite == false) ? "star-o" : "star"} onPress={()=>{this.favorite()}} color="#FFFFFF" size={scaling.favorite} backgroundColor="#FFFFFF00"/>
              </View>
            </View>
          </View>
          <View style={styles.bottomText}>
            <Text style={styles.poweredContainer}>
              <Text style={styles.poweredText}>Powered by:  </Text>
              <Text style={styles.poweredUrl} onPress={() => Linking.openURL("https://listen.moe/")}>LISTEN.moe</Text>
            </Text>
            <Text style={styles.listenersContainer}>
              <Text style={styles.listenersText}>Current Listeners:  </Text>
              <Text style={styles.listenersText}>{this.state.socket.listeners}</Text>
            </Text>
          </View>
        </Image>
        {(this.state.login == 0)?null:<TouchableOpacity style={styles.dialogShadow} onPress={()=>{this.setState({login: 0})}} activeOpacity={1.0}/>}
        {this.login()}
        {this.requestsFavorites()}
      </View>
    );
  }
  /**
  * Handles data from native code
  * @param {event} event event data
  */
  nativeEmitters(event: Event) {
    switch(event.name) {
      case "json":
        this.parseJSON(event.json);
        break;
      case "error":
        switch(event.error) {
          case "connect-error":
            this.setState({playingText: "Connection Error\n Retrying...", socket: {}});
            console.log("connect-error");
            setTimeout(() => {NativeInterface.startConnection()}, 5000)
            break;
          case "disconnected":
            this.setState({playingText: "Connection Error\n Retrying...", socket: {}});
            console.log("disconnected");
            setTimeout(() => {NativeInterface.startConnection()}, 5000)
            break;
          case "io-exception":
            this.setState({playingText: "Connection Error\n Retrying...", socket: {}});
            console.log("io-exception");
            setTimeout(() => {NativeInterface.startConnection()}, 5000)
            break;
          default:
            break;
          }
      case "favorite":
        let socket = this.state.socket;
        socket.extended.favorite = event.favorite;
        this.setState({socket: socket});
        break;
      case "login":
        this.setState({login: 1});
        break;
      case "status":
        this.setState({status: event.status});
        break;
      default:
        break;
      }
  }
  /**
  * Whether to show/what to show for - requested by text
  * @return {Component|null} Requested by text | null
  */
  showRequested() {
    if(this.state.socket.requested_by != '' && this.state.socket.requested_by != undefined) {
      let url = "https://forum.listen.moe/u/" + this.state.socket.requested_by;
      return (
        <Text style={styles.requestedContainer}>
          <Text style={styles.requestedText}>Requested by:  </Text>
          <Text style={styles.requestedUrl} onPress={() => Linking.openURL(url)}>{this.state.socket.requested_by}</Text>
        </Text>
      )
    } else {
      return null;
    }
  }
  /**
  * Whether to show/what to show for - anime text
  * @return {Component|null} Anime text | null
  */
  showAnime() {
    if(this.state.socket.anime_name != '' && this.state.socket.anime_name != undefined) {
      let anime = "[ " + this.state.socket.anime_name + " ]";
      return (
        <Text style={styles.playing}>{anime}</Text>
      )
    } else {
      return null;
    }
  }
  /**
  * Whether to show login dialog
  * @return {null|Component} null| Login component
  */
  login() {
    return (this.state.login == 0) ? null: (<Login onLogin={(token)=>{this.setState({login:0, authToken: token}); this.sendAuth(token)}} onCancel={()=>{this.setState({login:0})}}/>);
  }
  /**
  * Whether to show requests / favorites screen
  * @return {null|Component} null | Requests / Favorites component
  */
  requestsFavorites(){
    if(this.state.requestsFavorites == 0) return null;
    return (<RequestsFavorites page={this.state.requestsFavorites} onCancel={()=>{this.setState({requestsFavorites: 0})}}/>)
  }
  /**
  * Parses json from the WebSocket
  * @param {string} json Received json from socket
  */
  parseJSON(json) {
    json = JSON.parse(json);
    console.log("JSON Received");
    this.setState({playingText: "- Now Playing -", socket:json});
    if(json.extended == undefined) {
      console.log("Not Authenticated!");
      this.sendAuth();
    } else {
      console.log("Authenticated!");
    }
  }
  /**
  * Calls Native code to authenticate WebSocket
  * @param {string} token token from storage or from new login
  */
  async sendAuth(token) {
    let token = await api.getToken()
    console.log("Sending Authentication...");
    NativeInterface.sendAuthentication(token);
  }
  /**
  * Save the volume value
  * @param {float} value volume value
  */
  async saveVolume(value) {
    this.setState({volume: value});
    value *= 100;
    value = Math.round(value);
    try {
      await AsyncStorage.setItem("volume", value.toString());
    } catch(error) {}
  }
  /**
  * Retrieve the volume value
  */
  async getVolume() {
    try {
      const value = await AsyncStorage.getItem("volume");
      if(value != null) {
        value = parseInt(value);
        this.setState({volume: value / 100});
      }
    } catch(error) {}
  }
  /**
  * Toggles favorite status of currently playing song
  */
  async favorite() {
    let token = await api.getToken();
    if(token == undefined) {
      this.setState({login: 1});
    } else {
      if(this.state.socket.extended == undefined) {
        // TODO: Autheticate socket
      } else {
        let song = this.state.socket.song_id;
        let responseJSON = await api.favorite(song);
        if(responseJSON.success) {
          if(responseJSON.id == song) {
            let socket = this.state.socket;
            socket.extended.favorite = responseJSON.favorite;
            this.setState({socket: socket});
          }
        } else {
          // TODO Display an error
        }
      }
    }
  }
}
module.exports = Radio
