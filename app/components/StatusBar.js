'use strict'
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Platform,
} from 'react-native';

class CustomStatusBar extends Component {
  render() {
    if(Platform.OS == 'android'){
      return (
        <View>
          <StatusBar
            backgroundColor = "#EC1A55"
            barStyle = "light-content"
            hidden = {false}
          />
        </View>
      )
    } else {
      return (
        <View style={{height: 20, backgroundColor:"#EC1A55"}}/>
      )
    }
  }
}

module.exports = CustomStatusBar
