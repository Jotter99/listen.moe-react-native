package jcotter.listenmoe;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LISTENmoeService extends Service {
    static boolean isPlaying = false;
    static boolean isStarted = false;
    static boolean wsOpen = false;
    static boolean uiOpen = true;
    static float volume = 0.5f;

    static ReactApplicationContext reactContext;
    private WebSocket ws;
    private SimpleExoPlayer radioStream;
    private String json = "";
    private int songID = 0;
    private boolean favorite = false;
    private String token = "";

    private boolean isAuthenticated = false;
    int actionRequestCode = 0;

    public LISTENmoeService() {}
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        reactContext = NativeInterface.rc;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent.hasExtra(NativeInterface.INTENT_START)) {
            connectWebSocket();
            if(isPlaying) {
                WritableMap eventData = Arguments.createMap();
                eventData.putBoolean("status", true);
                eventEmitter("status", eventData);
            }
        } else if(intent.hasExtra(NativeInterface.INTENT_PLAY)) {
            if(!isStarted) {
                startStream();
            } else {
                radioStatus(intent.getBooleanExtra(NativeInterface.INTENT_PLAY,false));
            }
        } else if(intent.hasExtra(NativeInterface.INTENT_VOLUME)) {
            radioStream.setVolume(intent.getFloatExtra(NativeInterface.INTENT_VOLUME, 0.5f));
        } else if(intent.hasExtra(NativeInterface.INTENT_AUTH)) {
            sendAuthentication(intent.getStringExtra(NativeInterface.INTENT_AUTH));
        } else if(intent.hasExtra(NativeInterface.INTENT_STOP)) {
            radioStatus(false);
            if(ws != null && ws.isOpen() && !uiOpen) {
                ws.disconnect();
                stopSelf();
            } else {
                stopForeground(true);
                isStarted = false;
            }
        } else if(intent.hasExtra(NativeInterface.INTENT_FAV)) {
            favorite();
        } else if(intent.hasExtra(NativeInterface.INTENT_LOGIN)) {
            WritableMap eventData = Arguments.createMap();
            eventData.putString("error", "login");
            eventEmitter("error", eventData);
        }
        return START_NOT_STICKY;
    }

    /**
    * Sends data back to javascript
    * @param eventName Name to use when parsing event in javascript
    * @param eventData The data to be sent to javacript
    */
    private void eventEmitter(String eventName, WritableMap eventData) {
        eventData.putString("name", eventName);
        reactContext.getJSModule(RCTNativeAppEventEmitter.class).emit("native",eventData);
    }
    /**
    * Connects to WebSocket endpoint. Relays previous received json if socket is already open.
    * Returns data to javascript through eventEmitter()
    */
    public void connectWebSocket() {
        final String url = "wss://listen.moe/api/v2/socket";
        if(!json.equals("")) {
            if(NativeInterface.rc != null) {
                reactContext = NativeInterface.rc;
            }
            System.out.println("Already Open... Returning");
            WritableMap eventData = Arguments.createMap();
            eventData.putString("json", json);
            eventEmitter("json", eventData);
            return;
        }

        System.out.println("Creating Socket");
        ws = null;
        final WebSocketFactory factory = new WebSocketFactory();
        try {
            ws = factory.createSocket(url, 900000);
            ws.addListener(new WebSocketAdapter() {
                @Override
                public void onFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    wsOpen = true;
                    if (frame.getPayloadText().contains("listeners")) {
                        json = frame.getPayloadText();
                        if(json.contains("\"extended\":")) {
                            isAuthenticated = true;
                            favorite = new JSONObject(json).getJSONObject("extended").getBoolean("favorite");
                        }
                        WritableMap eventData = Arguments.createMap();
                        eventData.putString("json", json);
                        eventEmitter("json", eventData);
                        if(isStarted) {
                            updateNotification();
                        }
                    }
                }
                @Override
                public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
                    exception.printStackTrace();
                    json = "";
                    WritableMap eventData = Arguments.createMap();
                    eventData.putString("error", "connect-error");
                    eventEmitter("error", eventData);
                    ws = null;
                }
                @Override
                public void onMessageDecompressionError(WebSocket websocket, WebSocketException cause, byte[] compressed) throws Exception {
                    cause.printStackTrace();
                }
                @Override
                public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
                    if (closedByServer) {
                        WritableMap eventData = Arguments.createMap();
                        eventData.putString("error", "disconnected");
                        eventEmitter("error", eventData);
                        ws = null;
                        wsOpen = false;
                        json = "";
                    }
                }
            });
            // Connect to the socket //
            ws.connectAsynchronously();
        } catch (IOException ex) {
            if (ws.isOpen()) {
                ws.disconnect();
                ws = null;
                wsOpen = false;
            }
            WritableMap eventData = Arguments.createMap();
            eventData.putString("error", "io-exception");
            eventEmitter("error", eventData);
        }
    }
    /**
     * Authenticates the WebSocket Connection
     * @param token User authentication token
    */
    public void sendAuthentication(String token) {
        this.token = token;
        ws.sendText("{\"token\":\"" + token + "\"}");
    }
    /**
     * Plays/Pauses the radio stream
     * @param status play or pause
    */
    public void radioStatus(boolean status) {
        if(status) {
            radioStream.seekToDefaultPosition();
            radioStream.setVolume(volume);
        }
        isPlaying = status;
        radioStream.setPlayWhenReady(status);
        updateNotification();
        WritableMap eventData = Arguments.createMap();
        eventData.putBoolean("status", status);
        eventEmitter("status", eventData);
    }
    /**
     * Creates the radio stream
     */
    private void startStream() {
        final BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        final TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        final TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        final LoadControl loadControl = new DefaultLoadControl();
        final DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "LISTEN.moe"));
        final ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        final MediaSource streamSource = new ExtractorMediaSource(Uri.parse("https://listen.moe/stream"), dataSourceFactory, extractorsFactory, null, null);

        radioStream = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector, loadControl);
        radioStream.prepare(streamSource);
        isStarted = true;
        radioStatus(true);

        final IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                    radioStatus(false);
                }
            }
        };

        radioStream.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onPlayerError(ExoPlaybackException error) {
                radioStream.release();
                radioStream = null;
                startStream();
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }

            @Override
            public void onPositionDiscontinuity() {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                try {
                    if (playWhenReady) {
                        registerReceiver(broadcastReceiver, intentFilter);
                    } else {
                        unregisterReceiver(broadcastReceiver);
                    }
                } catch (IllegalArgumentException ignored) {
                }
            }
        });
    }
    /**
     * Update the notification
     */
    private void updateNotification() {
        if(!isStarted) return;
        try {
            JSONObject jsonObject = new JSONObject(json);
            StringBuilder titleBuilder = new StringBuilder();
            titleBuilder.append(jsonObject.get("song_name"));
            if(jsonObject.get("anime_name") != "") {
                titleBuilder.append(" [ ");
                titleBuilder.append(jsonObject.get("anime_name"));
                titleBuilder.append(" ]");
            }

            final String title = titleBuilder.toString();
            songID = jsonObject.getInt("song_id");
            actionRequestCode = 0;

            // Play/pause action
            final NotificationCompat.Action playPauseAction = new NotificationCompat.Action(
                    isPlaying ? R.drawable.ic_pause_black_24dp : R.drawable.ic_play_arrow_black_24dp,
                    isPlaying ? "Pause" : "Play",
                    getPlaybackActionService(NativeInterface.INTENT_PLAY, !isPlaying)
            );
            NotificationCompat.Action favoriteAction;
            if (isAuthenticated) {
                favoriteAction = new NotificationCompat.Action(
                        favorite ? R.drawable.ic_star_black_24dp : R.drawable.ic_star_border_black_24dp,
                        favorite ? "Unfavorite" : "Favorite",
                        getPlaybackActionService(NativeInterface.INTENT_FAV, true)
                );
            } else {
                favoriteAction = new NotificationCompat.Action(
                        R.drawable.ic_star_border_black_24dp,
                        "Favorite",
                        getPlaybackActionService(NativeInterface.INTENT_LOGIN, true)
                );
            }
            // Stop action
            final NotificationCompat.Action stopAction = new NotificationCompat.Action(
                    R.drawable.ic_stop_black_24dp,
                    "Stop",
                    getPlaybackActionService(NativeInterface.INTENT_STOP,true)
            );

            // Build the notification
            final Intent action = new Intent(getApplicationContext(), MainActivity.class);
            action.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            final PendingIntent clickIntent = PendingIntent.getActivity(getApplicationContext(), 0, action, PendingIntent.FLAG_UPDATE_CURRENT);

            final NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.icon_notification)
                    .setContentTitle(jsonObject.getString("artist_name"))
                    .setContentText(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentIntent(clickIntent)
                    .setOngoing(isStarted)
                    .setShowWhen(false)
                    .addAction(playPauseAction)
                    .addAction(favoriteAction)
                    .addAction(stopAction);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Visibility
                builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

                // Special media style
                builder.setStyle(new NotificationCompat.MediaStyle().setShowActionsInCompactView(0, 1, 2));

                // Text/icon color
                builder.setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            }

            startForeground(1, builder.build());
        } catch (JSONException ignored) {}
    }

    /**
     * Create the actions for the notification
     * @param action action
     * @param value value
     * @return Pending intent for the action
     */
    private PendingIntent getPlaybackActionService(final String action, final boolean value) {
        final Intent intent = new Intent(getApplicationContext(), LISTENmoeService.class);
        intent.putExtra(action, value);

        return PendingIntent.getService(getApplicationContext(), ++actionRequestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
    /**
     * Favorite function - located here as it is the easiest way to do it from JAVA
     * Luckily, React-Native supplies okhttp
     */
    private void favorite() {
        if(songID == 0 || token.equals("")) {
            WritableMap eventData = Arguments.createMap();
            eventData.putNull("login");
            eventEmitter("login", eventData);
            System.out.println("Native-Favorite Error. Probably not authenticated");
            return;
        }
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://listen.moe/api/songs/favorite")
                .addHeader("authorization", token)
                .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), "song=" + songID))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                if(result.contains("\"success\":true")) {
                    favorite = result.contains("\"favorite\":true");
                    updateNotification();
                    WritableMap eventData = Arguments.createMap();
                    eventData.putBoolean("favorite", favorite);
                    eventEmitter("favorite", eventData);
                }
            }
        });
    }
}
