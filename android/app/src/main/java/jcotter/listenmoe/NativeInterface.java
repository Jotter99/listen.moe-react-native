package jcotter.listenmoe;

import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

@SuppressWarnings("ConstantConditions")
class NativeInterface extends ReactContextBaseJavaModule{

    private static final String REACT_CLASS = "NativeInterface";

    static final String INTENT_START = ".start";
    static final String INTENT_STOP = ".stop";
    static final String INTENT_FAV = ".fav";
    static final String INTENT_LOGIN = ".login";
    static final String INTENT_VOLUME = ".volume";
    static final String INTENT_PLAY = ".play";
    static final String INTENT_AUTH = ".auth";
    static ReactApplicationContext rc;

    NativeInterface(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }
    /**
    * Starts the WebSocket
    */
    @ReactMethod
    public void startConnection() {
        rc = getReactApplicationContext();
        Intent intent = new Intent(getCurrentActivity(), LISTENmoeService.class)
                .putExtra(INTENT_START, true);
        getCurrentActivity().startService(intent);
    }
    /**
    * Authenticates the WebSocket Connection
    * @param token User authentication token
    */
    @ReactMethod
    public void sendAuthentication(String token) {
        Intent intent = new Intent(getCurrentActivity(), LISTENmoeService.class)
                .putExtra(INTENT_AUTH, token);
        getCurrentActivity().startService(intent);
    }
    /**
    * Play the radio stream
    * @param play Play or Pause
    */
    @ReactMethod
    public void playRadio(boolean play) {
        Intent intent = new Intent(getCurrentActivity(), LISTENmoeService.class)
                .putExtra(INTENT_PLAY, play);
        getCurrentActivity().startService(intent);
    }

    /**
     * Change volume of radio stream
     * @param volume float volume value
     */
    @ReactMethod
    public void updateVolume(float volume) {
        if(LISTENmoeService.isPlaying) {
            Intent intent = new Intent(getCurrentActivity(), LISTENmoeService.class)
                    .putExtra(INTENT_VOLUME, volume);
            getCurrentActivity().startService(intent);
        }else {
            LISTENmoeService.volume = volume;
        }
    }
}
