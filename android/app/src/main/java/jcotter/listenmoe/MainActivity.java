package jcotter.listenmoe;

import android.content.Intent;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;

public class MainActivity extends ReactActivity {
    @Override
    protected String getMainComponentName() {
        return "listenmoe";
    }

    @Override
    protected void onStop() {
        super.onStop();
        LISTENmoeService.uiOpen = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(LISTENmoeService.wsOpen) {
            Intent intent = new Intent(this, LISTENmoeService.class)
                    .putExtra(NativeInterface.INTENT_START, true);
            startService(intent);
        }
    }
}
